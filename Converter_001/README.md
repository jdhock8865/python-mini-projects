# CSV File Converter 001
## Author: John David Hock
## Created: 07/21/2019

Hello, thank you for using the CSV File Converter version 001

The purpose of this script is to convert data from a csv file in a specific format into a more readable format.
To use this converter the source data must consist of a csv file with two columns where column a has unique
values, such as zipcodes, and column b has non-unique values, such as cities.  See example below:

ZipCode|City
-------|----	      
30002|Avondale Estates
30003|Norcross
30004|Alpharetta
30005|Alpharetta
30006|Marietta
30007|Marietta
30008|Marietta
30009|Alpharetta
30010|Norcross
30011|Auburn
30012|Conyers

The script, once run, will convert the data from the source file into a new file with the format shown
below.  As seen below the new file now contains unique values from column b of the source file and a
list of the associated items, from column a, in column b.

City|ZipCode List
----|------------
Avondale Estates|30002
Norcross|30003 30010 30071 30091 30092 30093
Alpharetta|30004 30005 30009 30022 30023
Marietta|30006 30007 30008 30060 30061 30062 30063 30064 30065 30066 30067 30068 30069 30090


Directions
***********************************************************************************************************************
1. Before you run the script make sure source file is in the same directory as this script file and named 'source.csv'.
2. The output file will be named output.csv and it will be located in the same directory as the source file.
