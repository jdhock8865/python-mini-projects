# This project demonstrates the ability to use python to extract raw data from csv files
# and use that data to generate meaningful results.

# In this module, I will use a csv file that has a listing of each zipcode in the state of Georgia
# along with the city that the zipcode is assigned to.  I will use this data to generate a new csv file
# that has the city in column a and a list of zipcodes applicable to that city in column b.

import os
import csv
import sys
import itertools
import re

os.system('cls')

#notes file
notes = 'notes.txt'    

# Source File
filepath_read = 'source.csv'

# Target File
filepath_write = 'output.csv'

# Field Names from source file stored as a list
fieldnames_list = list()

# Custom Exceptions
class CustomError(Exception):
	pass

class InconsistentColumnNamesError(CustomError):
	pass

class Source_01IDColumnError(CustomError):
	pass

class DuplicateFileError(CustomError):
	pass

class NonNumericalDataError	(CustomError):
	pass

class InconsistentUniqueItems(CustomError):
	pass

class ExcessiveUniqueValuesInSource_02(CustomError):
	pass


# The create_list_from_file function returns a list of dictionaries containing the contents of the file passed to the function.
def create_list_from_file(filepath):
	temp_list_01 = list()
	try:
		rex = re.compile(r'^\s|\s$')
		with open(filepath, 'r', encoding='utf8') as f:
			f_reader = csv.DictReader(f, delimiter=',')
			fieldnames_list.append(f_reader.fieldnames)
			for data in f_reader:
				data.update({k: v.strip() for k, v in data.items() if isinstance(v, str) and rex.search(v)})
				temp_list_01.append(data)
		return temp_list_01

	except FileNotFoundError:
		print('ERROR!!!')
		print('source_01.csv file or source_02.csv file is missing.  Please make sure you have placed these files in the same directory as the python script file is in.')
		sys.exit()            


# The create_grouping_field_identifier_list function returns a list from the source files that is based on the 
# column that holds the values that data will be grouped on.
def create_grouping_field_identifier_list(source_list, check_len):
	try:
		temp_list_01 = list()
		temp_list_02 = list()
		for data in source_list:
			key_list = list(data.keys())
			unique_field_01 = data[key_list[0]]
			if unique_field_01 not in temp_list_01:
				temp_list_01.append(unique_field_01)
			unique_field_02 = data[key_list[1]]			
			if unique_field_02 not in temp_list_02:
				temp_list_02.append(unique_field_02)

		# check number of unique items in source_02.csv
		if check_len and len(temp_list_02) > 50:
			raise ExcessiveUniqueValuesInSource_02

		return temp_list_02
	
	except IndexError:
		print('ERROR!!!')
		print('There are columns missing from either source_01.csv or source_02.csv.')
		print('Please review the notes for this script and then verify the input files meet all required specifications.')
		sys.exit()

	except ExcessiveUniqueValuesInSource_02:
		print('Attention!!!')
		print('The number of unique items in column b of source_02.csv exceeds 50.')
		print('The resulting output will have over 150 columns of data!!')
		print('It is STRONGLY suggested that you split the data in the files into two or more files.')
		answer = input('Press Y to continue or any other key to cancel:')
		if answer == 'Y':
			return temp_list_02
		else:
			print('Program terminated at the request of user.')
			sys.exit()

# The create_groupedby_list function creates a list of dictionaries where the key and value come from the
# create_list_from_file function and the create_grouping_field_identifier_list function.  This function 
# uses those lists to create a new list that will be used to populate the output csv file.
def create_groupedby_list(source_data, grouping_data):    
	temp_list_01 = list()
	temp_list_02 = list()
	for grouping_field in grouping_data:
		for info in source_data:
			key_list = list(info.keys())
			key_field = key_list[1]
			group_field = key_list[0]
			search_field = info[key_field]
			if grouping_field == search_field:
				data_to_append = info[group_field]
				temp_list_01.append(data_to_append)
		strinfo = (f'{grouping_field}:{temp_list_01}')
		temp_list_02.append(strinfo)
		temp_list_01.clear()
	return (temp_list_02)


# The create_output_file function creates the output.csv file
def create_output_file(filepath, data_list):
    with open(filepath, 'w', encoding='utf8', newline='') as f:
        fieldnames = [fieldnames_list[0][1], fieldnames_list[0][0]]
        csv_file = csv.DictWriter(f, fieldnames, delimiter=',')
        csv_file.writeheader()
        data_list.sort()

        for info in data_list:
            key_field, value_list = info.split(':')
            temp_dict = {fieldnames_list[0][1]:key_field, fieldnames_list[0][0]: value_list}
            csv_file.writerow(temp_dict)
    return len(data_list)


with open(notes, 'r', encoding='utf8') as f:
        contents = f.read()
        print(contents)


print('\n')

answer = input('Press Y to continue or any other key to cancel:')

if answer == 'Y':
	source_list = create_list_from_file(filepath_read)
	source_grouping_values = create_grouping_field_identifier_list(source_list, False)
	output_list = create_groupedby_list(source_list, source_grouping_values)
	count = create_output_file(filepath_write, output_list)
	print('The output file has been successfully created and is ready for review!')
	print(f'{count} rows were procesed!')
else:
    print('Program terminated at the request of user.')
    sys.exit()