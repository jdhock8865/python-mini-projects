import os
import sys
import itertools

factors = [('0.496914026', '1269'), ('0.305621645', '638'), ('0.24011697', '2490'), ('0.358920479', '544')]
print(len(factors))

result_01 = list((float(a[0]) * int(a[1]) for a in factors))
result_02 = list(int(a[1]) for a in factors)

result = sum(result_01) / sum(result_02)
print(result_01)
print(result_02)
print(result)

mylist = [[1, 2, 3], [4, 8, 9], [5, 5, 8]]
mynew_set = set(mylist[2])
print(mynew_set)
print(len(mylist))