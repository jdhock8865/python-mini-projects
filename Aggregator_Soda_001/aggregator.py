# This project demonstrates the ability to use python to extract raw data from csv files
# and use that data to generate meaningful results.

# In this module, I will use two seperate csv files that contain related data to produce a new csv
# file that contians aggregated data.  Please see the README.md file for further information.

import os
import csv
import sys
import itertools
import re

os.system('cls')

#notes file
notes = 'notes.txt'    

# Source Files
filepath_01_read = 'source_01.csv'
filepath_02_read = 'source_02.csv'

# Target File
filepath_write = 'output.csv'

# Field Names from source file stored as a list
column_names = list()
fieldnames_list = list()

# Custom Exceptions
class CustomError(Exception):
	pass

class InconsistentColumnNamesError(CustomError):
	pass

class Source_01IDColumnError(CustomError):
	pass

class DuplicateFileError(CustomError):
	pass

class NonNumericalDataError	(CustomError):
	pass

class InconsistentUniqueItems(CustomError):
	pass

class ExcessiveUniqueValuesInSource_02(CustomError):
	pass

# The create_list_from_file function returns a list of dictionaries containing the contents of the file passed to the function.
def create_list_from_file(filepath):
	temp_list_01 = list()
	try:
		rex = re.compile(r'^\s|\s$')
		with open(filepath, 'r', encoding='utf8') as f:
			f_reader = csv.DictReader(f, delimiter=',')
			column_names.append(f_reader.fieldnames)
			for data in f_reader:
				data.update({k: v.strip() for k, v in data.items() if isinstance(v, str) and rex.search(v)})
				temp_list_01.append(data)
		return temp_list_01

	except FileNotFoundError:
		print('ERROR!!!')
		print('source_01.csv file or source_02.csv file is missing.  Please make sure you have placed these files in the same directory as the python script file is in.')
		sys.exit()            


# The create_grouping_field_identifier_list function returns a list of unique values from column b of the the source files.
def create_grouping_field_identifier_list(source_list, check_len):
	try:
		keys_list = list(source_list[0].keys())
		temp_list = list(set(map(lambda unique: unique.get(keys_list[1]), source_list)))

		# check number of unique items in source_02.csv
		if check_len and len(temp_list) > 50:
			raise ExcessiveUniqueValuesInSource_02

		return temp_list
	
	except IndexError:
		print('ERROR!!!')
		print('There are columns missing from either source_01.csv or source_02.csv.')
		print('Please review the notes for this script and then verify the input files meet all required specifications.')
		sys.exit()

	except ExcessiveUniqueValuesInSource_02:
		print('Attention!!!')
		print('The number of unique items in column b of source_02.csv exceeds 50.')
		print('The resulting output will have over 150 columns of data!!')
		print('It is STRONGLY suggested that you split the data in the files into two or more files.')
		answer = input('Press Y to continue or any other key to cancel:')
		if answer == 'Y':
			return temp_list
		else:
			print('Program terminated at the request of user.')
			sys.exit()


# The combine_data function returns a list that combines column b of source_01.csv with all three columns of
# source_02.csv
def combine_data(inner_list, outer_list):
	try:
		inner_list_keys = list(inner_list[0].keys())
		outer_list_keys = list(outer_list[0].keys())
		output_list = list()
		for x in outer_list:
			for y in inner_list:
				inner_list_id = x.get(inner_list_keys[0])
				outer_list_id = y.get(outer_list_keys[0])
				if outer_list_id == inner_list_id:
					data = [y.get(inner_list_keys[1]), outer_list_id, x.get(outer_list_keys[1]), x.get(outer_list_keys[2]), y.get(inner_list_keys[2])]
					output_list.append(data)

		# verify that column a in source_01.csv is labeled the same for column a is source_02.csv
		if (inner_list_keys[0] != outer_list_keys[0]):
			raise InconsistentColumnNamesError

		# verify that column a in source_01.csv contains unique values
		inner_zip_set = set(map(lambda zips: zips.get(inner_list_keys[0]), inner_list))
		outer_zip_set = set(map(lambda zips: zips.get(outer_list_keys[0]), outer_list))
		if len(inner_list) > len(inner_zip_set):
			raise Source_01IDColumnError

		# verify that source_01.csv and source_02.csv are not duplicate files
		inner_list_copy = inner_list.copy()
		outer_list_copy = outer_list.copy()
		if inner_list_copy == outer_list_copy:
			raise DuplicateFileError

		# verify that the data in column 3 of source_02.csv is numeric
		for data in outer_list:
			numerical_data_check = str(data.get(outer_list_keys[2]))
			try:
				float(numerical_data_check)
			except:
				raise NonNumericalDataError

		# verify that the number of unique items in column a of source_01.csv is equal to the number
		# of unique items is column a of source_02.csv.
		if (len(inner_zip_set) != len(outer_zip_set)):
			raise InconsistentUniqueItems

		return output_list

	except InconsistentColumnNamesError:
		print('ERROR!!! --- Column a in source_01.csv and source_02.csv must be labeled the same.')
		sys.exit()

	except Source_01IDColumnError:
		print('ERROR!!! --- Column a in source_01.csv does not contain unique values.')
		sys.exit()

	except DuplicateFileError:
		print('ERROR!!! --- source_01.csv and source_02.csv contain the same data.')
		sys.exit()

	except NonNumericalDataError:
		print('ERROR!!! --- Column c in source_02.csv must be numerical.')
		sys.exit()

	except InconsistentUniqueItems:
		print('Attention!!!')
		print('The number of unique items in column a of source_01.csv is does not equal the number of unique items in column a of source_02.csv.')
		print('You can continue with the aggregation but note that you will not have data for the missing unique items.')
		answer = input('Press Y to continue or any other key to cancel:')
		if answer == 'Y':
			return output_list
		else:
			print('Program terminated at the request of user.')
			sys.exit()

	
# The create_factor_groups converts the output of the combine_data function into a list that the sum, max,
# min functions can be applied to.
def create_factor_groups(grouping_list, factor_types, combined_data):
	temp_list_01 = list()
	factor_group_list = list()

	# This for loop combines the output of the factor_list list with the factors for the discounts in column c of source_02.csv.
	# The output is as follows:  Note that each factor maps to a zipcode
	# ['Dalton:Coke', '0.93803139', '0.386130254', '0.51271194', '0.004371668']
	# ['Dalton:Pepsi', '0.032442494', '0.849533147', '0.798324414', '0.590697492']
	# ['Dalton:Root Beer', '0.628094574', '0.966104738', '0.462406651', '0.405619225']
	factor_list = list(itertools.product(grouping_list, factor_types))
	for factortype in factor_list:
		id = factortype[0]
		factor_type = factortype[1]
		temp_list = [factortype]
		for data in combined_data:
			if data[0] == id and data[2] == factor_type:
				data_to_append = (data[3], data[4])
				temp_list.append(data_to_append)
		temp_list_01.append(temp_list)		


	# This for loop produces the final output for the csv file.  It takes each city and groups the factor info together with that city.
	# Using the sample csv file, this means that each city will be grouped with the factor info for that city.
	# ['Dalton', {'Coke': ['0.93803139', '0.386130254', '0.51271194', '0.004371668']}, {'Pepsi': ['0.032442494', '0.849533147', '0.798324414', '0.590697492']}, {'Root Beer': ['0.628094574', '0.966104738', '0.462406651', '0.405619225']}, {'Good Student': [0]}]
	for grouping_field in grouping_list:
		temp_list = [grouping_field]
		for data in temp_list_01:
			id = data[0][0]
			factor_type = data[0][1]
			if grouping_field == id:
				data.pop(0)
				info = {factor_type: data}
				temp_list.append(info)
		
		factor_group_list.append(temp_list)
	return factor_group_list

# The create_agg_output function creates the output file that contains the aggregated data.
def create_agg_output(agg_data, factor_types):
	output_list = list()
	for data in agg_data:
		i = data.pop(0)
		temp_list_a = list()
		for factor_dict in data:
			for factor_type in factor_types:
				try:
					factor_list = factor_dict.get(factor_type)
					result_00 = list((float(a[0]) * int(a[1]) for a in factor_list))
					result_02 = list(int(a[1]) for a in factor_list)
					factor_percent = sum(result_00) / sum(result_02)
					temp_list_a.append(factor_percent)
				except:
					pass
		
		factor_sum = '{:.2f}%'.format(sum(temp_list_a) * 100)
		temp_list_a = list(map("{}%".format, ['{:.2f}'.format(x * 100) for x in temp_list_a]))
		temp_list_a.append(factor_sum)
		temp_list_a.insert(0, i)
		output_list.append(temp_list_a)
	
	fieldnames_list = factor_types.copy()
	fieldnames_list.insert(0, column_names[0][1])
	fieldnames_list.append('check')

	with open(filepath_write, 'w', encoding='utf8', newline='')	as f:
		csv_file = csv.DictWriter(f, fieldnames_list, delimiter=',')
		csv_file.writeheader()
		for data in output_list:
			temp_dict = dict(zip(fieldnames_list, data))
			csv_file.writerow(temp_dict)


with open(notes, 'r', encoding='utf8') as f:
        contents = f.read()
        print(contents) 


print('\n')

answer = input('Press Y to continue or any other key to cancel:')

if answer == 'Y':
	source_01_list = create_list_from_file(filepath_01_read)
	source_02_list = create_list_from_file(filepath_02_read)
	source_01_grouping_values = create_grouping_field_identifier_list(source_01_list, False)
	source_02_grouping_values = create_grouping_field_identifier_list(source_02_list, True)
	combined_data = combine_data(source_01_list, source_02_list)
	factor_groups = create_factor_groups(source_01_grouping_values, source_02_grouping_values, combined_data)
	create_agg_output(factor_groups, source_02_grouping_values)
	print('The output file has been sucessfully created in the active directory!')
else:
    print('Program terminated at the request of user.')
    sys.exit()         
