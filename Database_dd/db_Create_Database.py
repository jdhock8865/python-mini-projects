#Reference https://stackoverflow.com/questions/15656445/python-create-access-database-using-win32com
#Reference https://docs.microsoft.com/en-us/office/client-developer/access/desktop-database-reference/dbengine-createdatabase-method-dao
#Reference http://timgolden.me.uk/pywin32-docs/html/com/win32com/HTML/QuickStartClientCom.html

import win32com.client
import os

os.system('cls')

dirpath = os.getcwd()
filename = 'dd.accdb'
filepath = os.path.join(dirpath, filename)

if os.path.exists(filepath):
    os.remove(filepath)

oAccess = win32com.client.Dispatch('Access.Application')
dbLangGeneral = ';LANGID=0x0409;CP=1252;COUNTRY=0'
# dbVersion40 64
dbVersion = 64
oAccess.DBEngine.CreateDatabase(filepath, dbLangGeneral, dbVersion)
oAccess.Quit()
del oAccess

if os.path.exists(filepath):
    print('The Database was successfully created!!')
else:
    print(f'ERROR!! Unable to create the {filename} database!')
