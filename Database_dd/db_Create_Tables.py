#References https://docs.microsoft.com/en-us/sql/odbc/microsoft/microsoft-access-data-types?view=sql-server-2017

import os
import pyodbc
import csv
from utilities import get_sql_statements

os.system('cls')

sql_statements = get_sql_statements()

driver = '{Microsoft Access Driver (*.mdb, *.accdb)}'

dirpath = os.getcwd()
filename = 'dd.accdb'
filepath = os.path.join(dirpath, filename)

conn_string = pyodbc.connect(driver=driver, dbq=filepath, autocommit=True)
cursor = conn_string.cursor()

# Create Database Tables
for data in sql_statements:
    table_name = data.get('table_name')
    drop_statement = data.get('drop_statement')
    create_statement = data.get('create_statement')

    try:
        cursor.execute(drop_statement)
    except:
        pass

    cursor.execute(create_statement)        


