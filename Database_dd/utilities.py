#References https://docs.microsoft.com/en-us/sql/odbc/microsoft/microsoft-access-data-types?view=sql-server-2017

import os
import pyodbc
import csv

os.system('cls')

dirpath = os.getcwd()
filename = r'data\sql.csv'
filepath = os.path.join(dirpath, filename)

sql_list = list()

def get_sql_statements():
   with open(filepath, 'r', encoding='utf8') as csv_file:
    sqlData = csv.DictReader(csv_file, delimiter=',')
    for sql_string in sqlData:
        sql_list.append(sql_string)

    return sql_list