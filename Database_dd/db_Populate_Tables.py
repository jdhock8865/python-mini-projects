#References https://docs.microsoft.com/en-us/sql/odbc/microsoft/microsoft-access-data-types?view=sql-server-2017

import os
import pyodbc
import csv

os.system('cls')

driver = '{Microsoft Access Driver (*.mdb, *.accdb)}'

dirpath = os.getcwd()
filename = 'dd.accdb'
filepath = os.path.join(dirpath, filename)

conn_string = pyodbc.connect(driver=driver, dbq=filepath, autocommit=True)
cursor = conn_string.cursor()