CSV File Converter 002
Author: John David Hock
Created: 07/11/2019
************************************************************************************************************************

Hello, thank you for using the CSV File Converter version 001

The purpose of this script is to convert data from a csv file in a specific format into a more readable format.
To use this converter the source data must consist of a csv file with three columns with the following criteria.
Column A must contain unique values.
Column B may or may not contain unique values.  Column B is used to perform the grouping.
Column C must contain numerical data because it will be summed based on column A and column B
In the example csv file below, column A has zipcodes, column B has cities, and column C has population for each zipcode

ZipCode|City|Population
-------|----|----------	      
30002|Avondale Estates|507
30003|Norcross|776
30004|Alpharetta|588
30005|Alpharetta|2365
30006|Marietta|1029
30007|Marietta|1013
30008|Marietta|796
30009|Alpharetta|1324
30010|Norcross|1559
30011|Auburn|1497
30012|Conyers|584


The script, once run, will convert the data from the source file into a new file with the format shown
below.  As seen below the new file now contains unique values from column b of the source file and a
list of the associated items, from column a, in column b.

City|Total|PopulationZipCode List
----|-----|----------------------
Avondale Estates|697|30002:697
Norcross|3174|30003:985 30010:65 30071:167 30091:87 30092:781 30093:1089
Alpharetta|4712|30004:1504 30005:809 30009:980 30022:654 30023:765


Directions
***********************************************************************************************************************
1. Before you run the script make sure source file is in the same directory as this script file and named 'source.csv'.
2. The output file will be named output.csv and it will be located in the same directory as the source file.