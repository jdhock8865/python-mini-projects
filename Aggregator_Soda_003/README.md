# CSV File Aggregator 002
## Author: John David Hock
## Created: 07/21/2019

Hello, thank you for using the CSV File Aggregator version 002

The purpose of this script is to aggredata data from two different csv files.  In this example, the two source
csv files have the same format as they did in CSV Aggregator version 001, however, this script produces a differnt
output where I am obtaining a weighted average for items in source_02.csv.

To calculate the weighted average for each item, which in the supplied source files is a popular soft drink,
I am using the following formula.

Using some sample data:

City|ZipCode|Soda:Percentage Favored|Population|Population * Percentage Favored
----|-------|-----------------------|----------|-------------------------------
Pineville|30002|Coke:0.28675|508|145.669
Pineville|30002|Pepsi:0.48631|508|247.04548
Pineville|30002|RootBeer:0.22694|508|115.28552
Pineville|30003|Coke:0.51632|716|369.68512
Pineville|30003|Pepsi:0.38726|716|277.27816
Pineville|30003|Root Beer:0.09642|716|69.03672


((508 * 0.28675) + (716 * 0.51632)) / (508 + 716) = .4210           42.10% for Coke
((508 * 0.48631) + (716 * 0.38726)) / (508 + 716) = .4284           42.84% for Pepsi
((508 * 0.22694) + (716 * 0.09642)) / (508 + 716) = .1505           15.05% for Root Beer

Total of Percentages: 99.99%

CSV File 1: 
Column A must contain unique values.
Column B must contain non unique values.
Column B is used to perform the grouping.

CSV File 2
Column A must contain non-unique values from Column A of CSV File 1.
Column B must contain labels for data in column C.
Note that the number of labels determines the number of coloumns in the output, for every label 3 columns will be generated.
Column C must contain numerical data.


CSV File 1 contains zipcodes and cities from the state of Georgia.
CSV file 2 contains the same zipcodes from csv file 1 along with three different popular soft drinks.


CSV File 1 Example

ZipCode|City
-------|----	      
30002|Avondale Estates|3215
30003|Norcross|485
30004|Alpharetta|1487
30005|Alpharetta|3587
30006|Marietta|14203
30007|Marietta|4215
30008|Marietta|6258
30009|Alpharetta|9857
30010|Norcross|1578
30011|Auburn|985
30012|Conyers|1502

CSV File 2 Example

ZipCode|FactorType|FactorValue
-------|----------|-----------
30002|Coke|0.3850
30002|Pepsi|0.4150
30002|Root Beer|0.2000
30003|Coke|0.3874
0003|Pepsi|0.2258
30003|Root Beer|0.3868


It is important that the sum of the individual factors for each zipcode equal 1 for this script to 
produce correct results. So in the example above you will note that for 30001 the sum of the factors
is equal to 1.

The script, once run, will convert the data from the two source files into a new file with the format shown below.
The check column is used to confirm that the sum of the percentages for each item being evaluated is equal to 1.

City|Coke|Pepsi|Root Beer|check
----|----|-----|---------|-----
Avondale Estates|68.00%|21.00%|11.00%|100.00%
Norcross|77.00%|19.00%|4.00%|100.00%
Apharetta|84.00%|13.00%|3.00%|100.00%


The first column header comes from column b of source_01.csv


Directions
***************************************************************************************************************
1. Before you run the script make sure that the source files are in the same directory as this script file.
2. The source file with the unique values must be named source_01.csv.
3. The source file with the data to be aggregated must be named source_02.csv.
4. Column a of both source files must be labeled the same.
5. Column c of source_02.csv must contain numerical data.
