import os
import sys
import itertools

factors = [('0.496914026', '1269'), ('0.305621645', '638'), ('0.24011697', '2490'), ('0.358920479', '544')]
print(len(factors))

result_01 = list((float(a[0]) * int(a[1]) for a in factors))
result_02 = list(int(a[1]) for a in factors)

result = sum(result_01) / sum(result_02)
print(result_01)
print(result_02)
print(result)

mylist = [[1, 2, 3], [4, 8, 9], [5, 5, 8]]
mynew_set = set(mylist[2])
print(mynew_set)
print(len(mylist))

# OrderedDict([('ZipCode', '31906'), ('City', 'Columbus'), ('Population', '703'), ('Year', '2017')])
# OrderedDict([('ZipCode', '31907'), ('City', 'Columbus'), ('Population', '649'), ('Year', '2017')])
# OrderedDict([('ZipCode', '31908'), ('City', 'Columbus'), ('Population', '2254'), ('Year', '2017')])
# OrderedDict([('ZipCode', '31909'), ('City', 'Columbus'), ('Population', '2338'), ('Year', '2017')])
# OrderedDict([('ZipCode', '31914'), ('City', 'Columbus'), ('Population', '639'), ('Year', '2017')])
# OrderedDict([('ZipCode', '31917'), ('City', 'Columbus'), ('Population', '1494'), ('Year', '2017')])
# OrderedDict([('ZipCode', '31993'), ('City', 'Columbus'), ('Population', '1336'), ('Year', '2017')])
