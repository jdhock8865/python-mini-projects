# This project demonstrates the ability to use python to extract raw data from csv files
# and use that data to generate meaningful results.

# In this module, I will use a csv file that has a listing of each zipcode in the state of Georgia
# along with the city that the zipcode is assigned to.  I will use this data to generate a new csv file
# that has the city in column a and a list of zipcodes applicable to that city in column b.

import os
import csv

os.system('cls')

# Source File
dirpath = r'H:\Factors\Source Data'
filename = 'Zip_Cities.csv'    
filepath_read = os.path.join(dirpath, filename)

# Target File
dirpath = r'H:\Factors\Source Data'
filename = 'cities_with_zipcodes.csv'    
filepath_write = os.path.join(dirpath, filename)

# Processing Lists
unique_city_list = list()
zip_cities_list = list()
zip_list = list()
city_zip_list = list()
city_zip_dict = dict()

def file_status(file):
    if file.closed == True:
        return 'Closed'
    else:
        return 'Open'

# Create the unique city list
try:
    with open(filepath_read, 'r', encoding='utf8') as f:
        f_reader = csv.DictReader(f, delimiter=',')
        for data in f_reader:
            city_name = data['City']
            zip_cities_list.append(data)
            if city_name not in unique_city_list:
                unique_city_list.append(city_name)

except KeyError as e:
    print(f"The column name of {e} does not exist in the csv file you are attempting to read!")

except FileNotFoundError as e:
    print('File Not Found')            

except Exception as e:
    print(f'The following error occurred when attempting to open the requested file for reading: {e}')

# Create the unique city list with zipcodes
for city in unique_city_list:
    for data in zip_cities_list:
        city_name = data['City']
        if city == city_name:
            zip_list.append(data['ZipCode'])
    strinfo = (f'{city}:{zip_list}')
    print(strinfo)
    city_zip_list.append(strinfo)            
    zip_list.clear()

#city_zip_list.sort()

# Save the unique city list with zipcodes to a csv file
with open(filepath_write, 'w', encoding='utf8', newline='') as f:
    fieldnames = ['City','ZipCodes']
    cities_with_zips = csv.DictWriter(f, fieldnames, delimiter=',')
    cities_with_zips.writeheader()

    for item in city_zip_list:
        city, list = item.split(':')
        temp_dict = dict(City=city, ZipCodes=list)
        cities_with_zips.writerow(temp_dict)

with open(filepath_write, 'r', encoding='utf8') as f:
    f_reader = csv.DictReader(f, delimiter=',')
    for data in f_reader:
        print(data)       

print(f'Number of records processed: {len(city_zip_list)}')
print(f'File Status: {file_status(f)}')
    
        
