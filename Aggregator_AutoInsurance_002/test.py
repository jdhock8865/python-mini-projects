import os
import sys
import itertools

numbers = ['3', '8', '9', '2']
factor_types = ['PCR', 'CPPT', 'MedMgt']
agg_types = ['High', 'Low', 'Avg']

t = itertools.product(factor_types, agg_types)
for data in t:
    column_name = data[0] + ' ' + data[1]
    print(column_name)

thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

if isinstance(thisdict,dict):
    print(True)
else:
    print(False)

keys = ['fname', 'lname']    
info = ['David', 'Hock']
mydict = dict(zip(keys, info))
print(mydict)

mynumber = 235699
print(str(mynumber).isnumeric())

mylist = [1, 2, 3, 4]
print(mylist.__doc__())
