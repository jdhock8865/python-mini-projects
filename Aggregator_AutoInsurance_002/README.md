# CSV File Aggregator 001
## Author: John David Hock
## Created: 07/21/2019

Hello, thank you for using the CSV File Aggregator version 001

The purpose of this script is to aggregate data from two different csv files.

CSV File 1: 
Column A must contain unique values.
Column B must contain non unique values.
Column B is used to perform the grouping.

CSV File 2
Column A must contain non-unique values from Column A of CSV File 1.
Column B must contain labels for data in column C.  Note that the number of labels determines the number of coloumns in the output, for every label 3 columns will be generated.
Column C must contain numerical data.


CSV File 1 contains zipcodes and cities from the state of Georgia.
CSV file 2 contains the same zipcodes along with three different types of common discounts that you may recieve on an auto insurance policy.

CSV File 1 Example

ZipCode|City
-------|----	      
30002|Avondale Estates
30003|Norcross
30004|Alpharetta
30005|Alpharetta
30006|Marietta
30007|Marietta
30008|Marietta
30009|Alpharetta
30010|Norcross
30011|Auburn
30012|Conyers

CSV File 2 Example

ZipCode|FactorType|FactorValue
-------|----------|-----------
30002|Safe Driver|0.552872376
30002|Multi Policy|0.302531811
30002|Low Mileage|0.037793375
30003|Safe Driver|0.781831788
30003|Multi Policy|0.635207468
30003|Low Mileage|0.146443505
30004|Safe Driver|0.601520583
30004|Multi Policy|0.636732115



The script, once run, will convert the data from the two source files into a new file with the format shown below.

City|Safe Driver Avg|Safe Driver Low|Safe Driver High|Multi Policy Low|Multi Policy High|Multi Policy Avg|Low Mileage Avg|Low Mileage Low|Low Mileage High
----|---------------|---------------|----------------|----------------|-----------------|----------------|---------------|---------------|----------------
Avondale Estates|0.5356|0.2486|0.8956|0.4875|0.3214|0.8754|0.4785|0.2498|0.6875
Norcross|0.5698|0.1257|0.7456|0.6325|0.3589|0.9875|0.5469|0.2274|0.8771
Apharetta|0.6510|0.4189|0.9956|0.3265|0.0254|0.4785|0.5557|0.3891|0.7485

The first column header comes from column b of source_01.csv


Directions
***************************************************************************************************************
1. Before you run the script make sure that the source files are in the same directory as this script file.
2. The source file with the unique values must be named source_01.csv.
3. The source file with the data to be aggregated must be named source_02.csv.
4. Column a of both source files must be labeled the same.
5. Column c of source_02.csv must contain numerical data.